package com.datougames.mpuzzle;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

import com.datougames.mpuzzle.mainGame.MainGameActivity;
import com.datougames.mpuzzle.userData.UpdateUserData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class LevelsActivity extends AppCompatActivity {

    private static final String TAG = "LevelsActivity: ";
    private FirebaseFirestore db;
    private UpdateUserData updateUserData;
    private String userId;
    private int currentLevel;
    private MainGameActivity mainGameActivity;
    private Intent musicIntent;

    public static LevelsActivity instance;
    //Initialize number of levels in case that there is any reading problem from firestore
    public int maximumPossibleLevel = 12;

    private static GridLayout gridLayout;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
        mainGameActivity = MainGameActivity.instance;
        db = FirebaseFirestore.getInstance();
        userId = MainMenuActivity.instance.getPersonId();
        updateUserData = new UpdateUserData(db);
        maximumPossibleLevel = MainMenuActivity.instance.levelSettings.getLevelSettings().get("maximumNumberOfLevels");

        try {
            //identification number = 2 means that the request for read in db comes from MainGameActivity
            updateUserData.getDocument(userId, 2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        musicIntent = new Intent(LevelsActivity.this, BackgroundSoundService.class);
        startMusicService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMusicService();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getCurrentLevel(long currentLevel){
        this.currentLevel = (int)currentLevel;
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_levels);
        loadBannerAd();
        initializeLevelsButtons();
    }

    public void loadButtonSelectedLevel(View view){
        Button button = (Button) view;
        //Split the text with the whitespace regex
        String[] splitedText = button.getText().toString().split("\\s+");

        int selectedLevel = Integer.parseInt(splitedText[1]);

        if (selectedLevel <= currentLevel){

            Intent intent = new Intent(this, MainGameActivity.class);
            intent.putExtra("mustReadFromDB", false);
            intent.putExtra("currentLevel", (long)selectedLevel);

            startActivity(intent);
            finish();
        }
        else {
        }
    }

    public void onBackButtonPressed(View backButtonView){
        finish();
    }

    public void startMusicService(){
        //startService(musicIntent);
        bindService(musicIntent, MainMenuActivity.instance.connection, Context.BIND_AUTO_CREATE);
    }

    public void stopMusicService(){
        //stopService(musicIntent);
        unbindService(MainMenuActivity.instance.connection);
    }

    @SuppressLint("SetTextI18n")
    public void initializeLevelsButtons(){
        int padding = 15;
        float textSize = 20;

        double inches = MainMenuActivity.instance.getMetrics();

        gridLayout = (GridLayout) findViewById(R.id.GridLayout);

        if (inches >= 7){
            gridLayout.setColumnCount(4);
        }
        else {
            gridLayout.setColumnCount(3);
            textSize = 15;
        }

        for (int i = 1; i <= maximumPossibleLevel; i++){

            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            //layoutParams.rightMargin = 0;
            layoutParams.leftMargin = 30;
            //layoutParams.topMargin = 60;
            layoutParams.bottomMargin = 30;
            //layoutParams.setMarginEnd(10);
            layoutParams.setGravity(Gravity.CENTER);

            Button levelButton = new Button(LevelsActivity.instance);

            if (i < 10) levelButton.setText("LEVEL 0" + i);
            else levelButton.setText("LEVEL " + i);

            levelButton.setTextColor(Color.rgb(0, 0,0));
            //levelButton.setPadding(padding*4, padding, padding*4,padding*2);
            levelButton.setTextSize(textSize);

            if (i > currentLevel){
                if (inches >= 7){
                    levelButton.setBackgroundResource(R.drawable.level_red_buttons_background);
                }
                else {
                    levelButton.setBackgroundResource(R.drawable.level_red_buttons_background_small);
                }
            }
            else {
                if (inches >= 7){
                    levelButton.setBackgroundResource(R.drawable.level_buttons_background);
                }
                else {
                    levelButton.setBackgroundResource(R.drawable.level_buttons_background_small);
                }
            }

            levelButton.setOnClickListener(view -> loadButtonSelectedLevel(levelButton));

            gridLayout.addView(levelButton, layoutParams);
        }
    }

    public void loadBannerAd(){
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

}
