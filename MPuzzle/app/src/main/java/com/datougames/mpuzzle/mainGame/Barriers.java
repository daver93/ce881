package com.datougames.mpuzzle.mainGame;

import java.util.Random;

public class Barriers {

    private final int idForBarriers = 99;
    private int level;
    private int [][] table;
    private boolean isMarathon;

    //NOTE: The level number is also the dimension number of the table
    public Barriers(int level, int[][] table){
        this.level = level;
        this.table = table;
    }

    public void setBarriersPosition(boolean isMarathon){

        this.isMarathon = isMarathon;
        findPathPosition();

        if (isMarathon){
            findDynamicallyBarriersPosition(12);
        }
        else {
            if (level == 1){
                //The first level is just a simple intro to the game and there in no barriers
            }
            else if (level == 2){
                table[0][1] = idForBarriers;
            }
            else if (level == 3){
                table[1][0] = idForBarriers;
                table[0][2] = idForBarriers;
                table[2][1] = idForBarriers;
            }
            else {
                findDynamicallyBarriersPosition(level);
            }
        }
    }

    private void findDynamicallyBarriersPosition(int numberOfBarriers){
        int currentNumberOfBarriers = 0;
        int totalNumberOfBarriers = numberOfBarriers;

        if (numberOfBarriers >= 5 && numberOfBarriers < 8){
            totalNumberOfBarriers *= 2;
        }
        else if (numberOfBarriers >= 8){
            totalNumberOfBarriers *= 3;
        }

        //in each level (after level 3), the number of barriers will be equal to the number of the level times a number of times
        while (currentNumberOfBarriers < totalNumberOfBarriers){
            Random random = new Random();
            //random integer number between 0 and length of the table
            int randomPositionX = random.nextInt(table.length);
            int randomPositionY = random.nextInt(table.length);

            if (table[randomPositionX][randomPositionY] == 0){
                table[randomPositionX][randomPositionY] = idForBarriers;
                currentNumberOfBarriers++;
            }
        }
    }

    public int getIdForBarriers(){
        return this.idForBarriers;
    }

    public void findPathPosition(){

        if (level > 3 || isMarathon){
            int i = 0;
            int j = 0;

            //while (table[i][j] != MainGameModel.instance.winPosition){
            while (i < table.length-1 || j < table.length-1){
                Random random = new Random();
                //random integer number between 0 and 1
                int randomPosition = random.nextInt(2);

                // 0 is right
                if (randomPosition == 0){
                    int temp = j + 1;
                    if (temp < MainGameModel.instance.size){
                        j = temp;
                        if (table[i][j] != MainGameModel.instance.winPosition){
                            table[i][j] = MainGameModel.instance.pathPosition;
                        }
                    }
                }

                //1 is left
                else if (randomPosition == 1) {
                    int temp = i + 1;
                    if (temp < MainGameModel.instance.size){
                        i = temp;
                        if (table[i][j] != MainGameModel.instance.winPosition){
                            table[i][j] = MainGameModel.instance.pathPosition;
                        }
                    }
                }
            }
        }
    }

}
