package com.datougames.mpuzzle.hallOfFame;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.datougames.mpuzzle.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.LinkedHashMap;
import java.util.Map;

public class HallOfFameActivity extends AppCompatActivity {

    public static HallOfFameActivity instance;
    private static GridLayout gridLayout;
    private static final String TAG = "HallOfFameActivity: ";
    private AdView mAdView;

    private FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_hall_of_fame);

        loadBannerAd();

        db = FirebaseFirestore.getInstance();

        HallOfFameData hallOfFameData = new HallOfFameData(db);
    }

    public void loadBannerAd(){
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void initializeHallOfFame(LinkedHashMap<String, Integer> hallTable){

        gridLayout = (GridLayout) findViewById(R.id.GridLayout);

        int id = 0;
        for (Map.Entry<String, Integer> entry : hallTable.entrySet()) {
            id++;

            String key = entry.getKey();
            int value = entry.getValue();

            TextView userNameText = new TextView(HallOfFameActivity.instance);
            createTextFields(userNameText, key, null, 15, 20, Typeface.BOLD, id);

            TextView bestScoreText = new TextView(HallOfFameActivity.instance);
            createTextFields(bestScoreText, Integer.toString(value), null, 15, 20, Typeface.BOLD, id);
        }
        setTheme(R.style.AppTheme);
    }

    public void createTextFields(TextView textView, CharSequence text, Color color, int padding, int textSize, int typeface, int counter){

        textView.setText(text);
        textView.setId(counter);
        textView.setAllCaps(false);
        textView.setPadding(padding*4, padding*1, padding*4,padding*2);
        textView.setTextSize(textSize);
        textView.setTypeface(null, typeface);
        textView.setTag(textView.getText());
        //textView.setTextColor(Color.BLUE);
        textView.setTextSize(30);

        gridLayout.addView(textView);

    }

    public void onBackButtonPressed(View view){
        finish();
    }


    //Notes: For my Banner :  ads:adUnitId="ca-app-pub-0130784657161358/7030090120"
            // For the test Banner: ca-app-pub-3940256099942544/6300978111
}
