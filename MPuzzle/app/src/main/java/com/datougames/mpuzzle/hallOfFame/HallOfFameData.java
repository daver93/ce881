package com.datougames.mpuzzle.hallOfFame;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedHashMap;

public class HallOfFameData {

    private final static int NUMBER_OF_BEST_SCORES = 15;

    private static final String TAG = "HallOfFameData ";
    private final FirebaseFirestore db;
    private LinkedHashMap<String, Integer> hallTable;

    public HallOfFameData(FirebaseFirestore db) {
        this.db = db;
        setup();

        getUserScores();
    }

    public void setup() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        db.setFirestoreSettings(settings);
    }

    public void getUserScores(){

        db.collection("marathonData")
                .orderBy("maximumReachedLevel", Query.Direction.DESCENDING) //order the results from biggest to lowest score
                .limit(NUMBER_OF_BEST_SCORES)  // Get the first NUMBER_OF_BEST_SCORES (15) results (after ordering them)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            hallTable = new LinkedHashMap<String, Integer>();

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                String userName = (String) document.get("userDisplayName");
                                int bestScore = Integer.parseInt(String.valueOf(document.get("maximumReachedLevel")));

                                hallTable.put(userName, bestScore);
                            }

                            updateHallOfFameActivity();

                        } else {

                        }
                    }
                });
    }

    public void updateHallOfFameActivity(){
        HallOfFameActivity.instance.initializeHallOfFame(hallTable);
    }

}
