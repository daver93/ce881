package com.datougames.mpuzzle.marathon;

import androidx.annotation.NonNull;

import com.datougames.mpuzzle.mainGame.MainGameActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.util.HashMap;
import java.util.Map;

public class MarathonData {

    private final FirebaseFirestore db;
    private static final String TAG = "MarathonData: ";

    public String userId;
    public String userDisplayName;
    public int maximumReachedLevel;

    public MarathonData(FirebaseFirestore db){
        this.db = db;
        setup();
    }

    public void setup() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        db.setFirestoreSettings(settings);
    }

    public void createNewDocument(){
        //When the document is being created, the user does not have any best score yet in the Marathon Mode
        maximumReachedLevel = 0;

        Map<String, Object> userData = new HashMap<>();
        userData.put("userID", userId);
        userData.put("userDisplayName", userDisplayName);
        userData.put("maximumReachedLevel", maximumReachedLevel);

        db.collection("marathonData").document(userId)
                .set(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (userId != null){
                            startMarathonGame(1);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    public void getDocument(String userId, String userDisplayName, int levelNumber) throws InterruptedException {
        this.userId = userId;
        this.userDisplayName = userDisplayName;

        DocumentReference docRef = db.collection("marathonData").document(userId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        if (document.get("userID") != null){
                            startMarathonGame(levelNumber);
                        }
                        else {
                        }

                    } else {
                        createNewDocument();
                    }
                } else {
                }
            }
        });
    }

    public void startMarathonGame(int levelNumber){
        MainGameActivity.instance.setupActivity(levelNumber);
    }

    public void updateMaximumReachedLevel(int currentLevel){
        // Here we must check if the current level is greater that the one that is stored on Firebase
        //If yes, then the current level is the best score for this player, and Firebase must be updated with that info
        DocumentReference docRef = db.collection("marathonData").document(userId);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        int maxReachedLevelInDb = Integer.parseInt(String.valueOf(document.get("maximumReachedLevel")));

                        if (currentLevel > maxReachedLevelInDb){
                            updateFirebaseWithBestUserScore(currentLevel);
                        }
                    } else {
                    }
                } else {
                }
            }
        });
    }

    public void updateFirebaseWithBestUserScore(int newScore){
        Map<String, Object> userData = new HashMap<>();
        userData.put("userID", userId);
        userData.put("userDisplayName", userDisplayName);
        userData.put("maximumReachedLevel", newScore);

        db.collection("marathonData").document(userId)
                .set(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }
}
