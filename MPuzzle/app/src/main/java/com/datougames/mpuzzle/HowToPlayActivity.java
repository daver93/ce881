package com.datougames.mpuzzle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HowToPlayActivity extends AppCompatActivity {

    private Intent musicIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
    }

    @Override
    protected void onStart() {
        super.onStart();
        musicIntent = new Intent(HowToPlayActivity.this, BackgroundSoundService.class);
        startMusicService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMusicService();
    }

    public void startMusicService(){
        //startService(musicIntent);
        bindService(musicIntent, MainMenuActivity.instance.connection, Context.BIND_AUTO_CREATE);
    }

    public void stopMusicService(){
        //stopService(musicIntent);
        unbindService(MainMenuActivity.instance.connection);
    }

    public void onBackButtonPressed(View backButtonView){
        finish();
    }
}
