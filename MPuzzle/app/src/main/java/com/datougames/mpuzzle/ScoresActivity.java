package com.datougames.mpuzzle;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.datougames.mpuzzle.userData.UpdateUserData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class ScoresActivity extends AppCompatActivity {

    //Initialize number of levels in case that there is any reading problem from firestore
    private int maximumPossibleLevel = 12;
    private final String TAG = "ScoresActivity: ";
    private FirebaseFirestore db;
    private String userId;
    private Intent musicIntent;

    public UpdateUserData updateUserData;
    public static ScoresActivity instance;

    private static GridLayout gridLayout;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        maximumPossibleLevel = MainMenuActivity.instance.levelSettings.getLevelSettings().get("maximumNumberOfLevels");

        connectToFirestore();

        setTheme(R.style.AppTheme);
    }

    @Override
    protected void onStart() {
        super.onStart();
        musicIntent = new Intent(ScoresActivity.this, BackgroundSoundService.class);
        startMusicService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMusicService();
    }

    public void startMusicService(){
        //startService(musicIntent);
        bindService(musicIntent, MainMenuActivity.instance.connection, Context.BIND_AUTO_CREATE);
    }

    public void stopMusicService(){
        //stopService(musicIntent);
        unbindService(MainMenuActivity.instance.connection);
    }

    private void connectToFirestore(){
        db = FirebaseFirestore.getInstance();
        userId = MainMenuActivity.instance.getPersonId();
        updateUserData = new UpdateUserData(db);

        setContentView(R.layout.activity_scores);
        initializeScoreInfo();
        loadBannerAd();
        readScores();
    }

    public void onBackButtonPressed(View backButtonView){
        finish();
    }

    private void readScores(){
        for (int i = 1; i <= maximumPossibleLevel; i++){
            String textId = "score".concat(String.valueOf(i));

            // The levelScoreInfo have an Id bigger than 100
            TextView scoreText = (TextView) findViewById(100 + i);
            // With the tag we make sure that we read the correct levelScoreInfo TextView
            scoreText.findViewWithTag(textId);

            try {
                updateUserData.getScores(userId, String.valueOf(i), scoreText);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //When Firebase returns the score for a level, this method is called to update the UI
    public void updateScoresUI(TextView scoreText, String score){
        scoreText.setText("Score: " + score);
    }

    @SuppressLint("SetTextI18n")
    public void initializeScoreInfo(){
        gridLayout = (GridLayout) findViewById(R.id.GridLayout);

        double inches = MainMenuActivity.instance.getMetrics();

        int textSize = 30;

        // Check the size of the screen in order to decide the grid layout
        if (inches >= 7){
            gridLayout.setColumnCount(4);
        }
        else {
            gridLayout.setColumnCount(3);
            textSize = 20;
        }

        for (int i = 1; i <= maximumPossibleLevel; i++){

            LinearLayout groupInfoLayout = new LinearLayout(ScoresActivity.instance);
            groupInfoLayout.setOrientation(LinearLayout.VERTICAL);

            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            //layoutParams.rightMargin = 0;
            layoutParams.leftMargin = 30;
            //layoutParams.topMargin = 60;
            layoutParams.bottomMargin = 30;

            TextView levelNumberInfo = new TextView(ScoresActivity.instance);

            if (i < 10) levelNumberInfo.setText("LEVEL 0" + i);
            else levelNumberInfo.setText("LEVEL " + i);

            levelNumberInfo.setAllCaps(true);
            levelNumberInfo.setTextSize(textSize);
            levelNumberInfo.setTypeface(null, Typeface.BOLD);
            levelNumberInfo.setTag(levelNumberInfo.getText());
            levelNumberInfo.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            groupInfoLayout.addView(levelNumberInfo);

            TextView levelScoreInfo = new TextView(ScoresActivity.instance);

            // The levelScoreInfo have an Id bigger than 100 in order to be easier to identify them later
            levelScoreInfo.setId(100 + i);
            levelScoreInfo.setText("SCORE: Loading");
            levelScoreInfo.setAllCaps(true);
            levelScoreInfo.setTextSize(textSize-6);
            levelScoreInfo.setTypeface(null, Typeface.BOLD);
            // The tag is very important for the identification process later
            levelScoreInfo.setTag("score" + i);
            levelScoreInfo.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            groupInfoLayout.addView(levelScoreInfo);

            gridLayout.addView(groupInfoLayout, layoutParams);
        }
    }

    public void loadBannerAd(){
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
