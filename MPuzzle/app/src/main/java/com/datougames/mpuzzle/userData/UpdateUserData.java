package com.datougames.mpuzzle.userData;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.datougames.mpuzzle.LevelsActivity;
import com.datougames.mpuzzle.mainGame.MainGameActivity;
import com.datougames.mpuzzle.MainMenuActivity;
import com.datougames.mpuzzle.ScoresActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.util.HashMap;
import java.util.Map;

public class UpdateUserData {

    private final FirebaseFirestore db;

    private static final String TAG = "UpdateUserData";

    private UserData userData;

    public String userId;
    public long currentLevel;

    public UpdateUserData(FirebaseFirestore db){
        this.db = db;
        userData = new UserData();
        scoreForLevel = new HashMap<>();
        setup();
    }

    public void setup() {
        // [START get_firestore_instance]
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // [END get_firestore_instance]

        // [START set_firestore_settings]
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        db.setFirestoreSettings(settings);
        // [END set_firestore_settings]
    }

    //this number shows where the call for the read comes from
    private int identificationNumber;

    public void getDocument(String userId, int identificationNumber) throws InterruptedException {
        // [START get_document]

        this.identificationNumber = identificationNumber;
        this.userId = userId;

        DocumentReference docRef = db.collection("userData").document(userId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        if (document.get("userID") != null){
                            setCurrentLevel((long)document.get("level"));
                        }
                        else {
                            setCurrentLevel(1);
                        }

                    } else {
                        createNewDocument();
                        initializeScores();
                    }
                } else {
                }

            }
        });
        // [END get_document]
    }

    public void createNewDocument(){

        // [START set_document]

        currentLevel = 1;

        Map<String, Object> userData = new HashMap<>();
        userData.put("userID", userId);
        userData.put("level", currentLevel);

        db.collection("userData").document(userId)
                .set(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (userId != null){
                            setCurrentLevel(currentLevel);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
        // [END set_document]
    }

    public void initializeScores(){
        //if current level is 1, then player has not finished any level yet.

        for (int i = 1; i <= MainMenuActivity.instance.levelSettings.getLevelSettings().get("maximumNumberOfLevels"); i++){
            if (currentLevel == 1){
                setLevelScore(i, 0);
            }
            else {

            }

        }
    }

    Map<Integer, Integer> scoreForLevel;
    public void setLevelScore(int level, int score){
        scoreForLevel.put(level, score);

        setScoreDocuments(level);
    }

    public void setScoreDocuments(int documentLevel){
        Map<String, Integer> tempMap = new HashMap<>();
        tempMap.put(String.valueOf(documentLevel), scoreForLevel.get(documentLevel));

        db.collection("userData").document(userId).collection("scores").document(String.valueOf(documentLevel))
                .set(tempMap)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    public void setDocument() {
        // [START set_document]
        Map<String, Object> userData = new HashMap<>();
        userData.put("userID", userId);
        userData.put("level", currentLevel);

        db.collection("userData").document(userId)
                .set(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
        // [END set_document]

        MainGameActivity.instance.updateVisibleLevelInfo();
    }

    public void updateUserWithLevel(String userId, long currentLevel){
        this.userId = userId;
        this.currentLevel = currentLevel;

        setDocument();
    }

    public long getCurrentLevel(){
        return this.currentLevel;
    }

    public void setCurrentLevel(long currentLevel){
        this.currentLevel = currentLevel;

        if (identificationNumber == 1){
            MainGameActivity.instance.setupActivity(currentLevel);
        }
        else if (identificationNumber == 2){
            LevelsActivity.instance.getCurrentLevel(currentLevel);
        }
    }

    public void getScores(String userId, String levelNumberToStringFormat, TextView scoreText) throws InterruptedException {
        // [START get_document]

        this.userId = userId;
        String[] score = new String[1];

        DocumentReference docRef = db.collection("userData").document(userId).collection("scores").document(levelNumberToStringFormat);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        long retrievedScore = (long) document.get(levelNumberToStringFormat);
                        score[0] = Long.toString(retrievedScore);

                        ScoresActivity.instance.updateScoresUI(scoreText, score[0]);
                    } else {
                        score[0] = "0";
                        ScoresActivity.instance.updateScoresUI(scoreText, score[0]);
                    }
                } else {
                }
            }
        });
        // [END get_document]
    }

}
