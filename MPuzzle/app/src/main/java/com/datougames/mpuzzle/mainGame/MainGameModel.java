package com.datougames.mpuzzle.mainGame;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import com.datougames.mpuzzle.MainMenuActivity;

import java.util.Arrays;

public class MainGameModel {

    private static final String TAG = "MainGameModel: ";
    private long levelTimeLimit;
    private long remainingTime;
    private boolean askedRewardTime;
    private CountDownTimer countDownTimer;
    private TextView timerText;

    public static MainGameModel instance;
    public MainGameActivity mainGameActivity;
    public Barriers barriers;
    public int[][] table;
    public int size;
    public int levelNumber;
    public final int playerPosition = 9;
    public final int winPosition = 20;
    public final int pathPosition = 5;
    public int maximumPossibleLevel;

    public MainGameModel(int dimension, TextView timerText, int levelNumber){
        instance = this;
        this.levelNumber = levelNumber;
        mainGameActivity = MainGameActivity.instance;

        maximumPossibleLevel = MainMenuActivity.instance.levelSettings.getLevelSettings().get("maximumNumberOfLevels");

        this.timerText = timerText;
        this.size = dimension;
        askedRewardTime = false;
        setPositions();
    }

    public void stopCurrentTimeCounter(){
        countDownTimer.cancel();
    }

    public long getTimeLimit(){
        if (mainGameActivity.isMarathon){
            if (!askedRewardTime){
                levelTimeLimit = mainGameActivity.marathonTimeLimit;
            }
        }
        else {
            if (levelNumber == 1){
                levelTimeLimit = 6000;
            }
            else if (levelNumber >= 2 && levelNumber <= 8){
                levelTimeLimit = 8000;
            }
            else {
                levelTimeLimit = 9000;
            }
        }
        return levelTimeLimit;
    }

    public CountDownTimer createLiveTimer(final TextView timerText) {
        if (MainGameActivity.instance.isMarathon){

            if (askedRewardTime){
                askedRewardTime = false;
            }
            else {
                getTimeLimit();
            }

            return new CountDownTimer(levelTimeLimit, 1000) {
                @Override
                public void onTick(long millisecondUntilFinish) {
                    remainingTime = millisecondUntilFinish / 1000;
                    timerText.setText("TIME: " + Long.toString(remainingTime));
                }

                @Override
                public void onFinish() {
                    try{
                        remainingTime = 0;
                        timerText.setText("TIME: " + Long.toString(remainingTime));
                        if (remainingTime == 0){
                            mainGameActivity.askForRewardedTime();
                        }
                    }
                    catch (Exception e){
                    }
                }
            };
        }
        else {
            return new CountDownTimer(getTimeLimit(), 1000) {
                @Override
                public void onTick(long millisecondUntilFinish) {
                    remainingTime = millisecondUntilFinish / 1000;
                    timerText.setText("TIME: " + Long.toString(remainingTime));
                }

                @Override
                public void onFinish() {
                    try{
                        remainingTime = 0;
                        timerText.setText("TIME: " + Long.toString(remainingTime));
                        mainGameActivity.islost();
                    }
                    catch (Exception e){
                        System.out.println("EXCEPTION CAUGHT AT createLiveTimer(): " + Arrays.toString(e.getStackTrace()));
                    }
                }
            };
        }
    }

    public long getRemainingTime(){
        return remainingTime;
    }

    public void setPlayerPosition(String direction){
        for (int i = 0; i < table.length; i++){
            for (int j = 0; j < table.length; j++){
                if (table[i][j] == playerPosition){
                    if (direction.equals("down")){
                        if (j < table.length-1 && table[i][j+1] != barriers.getIdForBarriers()){
                            table[i][j] = 0;
                            int temp = ++j;
                            if (table[i][temp] == winPosition){
                                win();
                            }
                            table[i][temp] = playerPosition;
                        }
                    }
                    else if (direction.equals("right")){
                        if (i < table.length-1 && table[i+1][j] != barriers.getIdForBarriers()){
                            table[i][j] = 0;
                            int temp = ++i;
                            if (table[temp][j] == winPosition){
                                win();
                            }
                            table[i][j] = playerPosition;
                        }
                    }
                }
            }
        }
    }

    public void win(){
        stopCurrentTimeCounter();

        // Check if the player is playing the Marathon Mode
        if (mainGameActivity.isMarathon){
            mainGameActivity.marathonData.updateMaximumReachedLevel(levelNumber);
            mainGameActivity.mainGameView.createModelCorrespondingToLevel(timerText, ++levelNumber, true);

            mainGameActivity.currentLevel++;
            mainGameActivity.updateVisibleLevelInfo();
        }
        else {
            if (mainGameActivity.mustReadFromDB){
                //the final score for the current finished level is the remaining time
                mainGameActivity.updateUserData.setLevelScore(levelNumber , (int) remainingTime);
            }
            mainGameActivity.showFinishLevel();

            //Disable share function
            //mainGameActivity.shareFinishedLevel();
        }
    }

    public void resetGame(){

        stopCurrentTimeCounter();

        setPositions();
    }

    public void setPositions(){
        if (getTimeLimit() > 0){
            countDownTimer = createLiveTimer(timerText);
            countDownTimer.start();
        }

        table = new int[this.size][this.size];

        //9 is the identification for the table to know the position of the player
        table[0][0] = playerPosition;

        //20 is the identification for the table to know the position that the player must reach to win
        table[size-1][size-1] = winPosition;

        barriers = new Barriers(levelNumber, table);
        barriers.setBarriersPosition(MainGameActivity.instance.isMarathon);
    }

    // This method is being called when player choose to see a video ad for additional time
    public void giveRewardedTime(long rewardedTime){
        if (rewardedTime > 0){
            askedRewardTime = true;
            levelTimeLimit = rewardedTime;

            countDownTimer = createLiveTimer(timerText);
            countDownTimer.start();

            Log.d(TAG, "giveRewardedTime() CALLED with CURRENT_LEVEL: " + levelNumber);
            System.out.println("giveRewardedTime() CALLED with CURRENT_LEVEL: " + levelNumber);
        }
    }
}
