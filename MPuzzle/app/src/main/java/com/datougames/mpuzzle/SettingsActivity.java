package com.datougames.mpuzzle;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    private Intent musicIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
    }

    public void onBackButtonPressed(View backButtonView){
        finish();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        musicIntent = new Intent(SettingsActivity.this, BackgroundSoundService.class);
        startMusicService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMusicService();
    }

    public void startMusicService(){
        //startService(musicIntent);
        bindService(musicIntent, MainMenuActivity.instance.connection, Context.BIND_AUTO_CREATE);
    }

    public void stopMusicService(){
        //stopService(musicIntent);
        unbindService(MainMenuActivity.instance.connection);
    }
}