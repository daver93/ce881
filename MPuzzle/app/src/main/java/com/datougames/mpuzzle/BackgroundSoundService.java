package com.datougames.mpuzzle;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class BackgroundSoundService extends Service {

    private final IBinder binder = new LocalBinder();
    private final String TAG = "BackgroundSoundService";

    MediaPlayer mediaPlayer;


    public class LocalBinder extends Binder {
        BackgroundSoundService getService() {
            System.out.println("LocalBinder extends Binder");
            // Return this instance of LocalService so clients can call public methods
            return BackgroundSoundService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("onBind");
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mediaPlayer = MediaPlayer.create(this, R.raw.sound);
        mediaPlayer.setLooping(true); // Set looping
        mediaPlayer.setVolume(100, 100);
    }

    @Override
    public void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    public void pauseMusic(){
        if (mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            System.out.println("pauseMusic just called");
        }
    }

    public void continueMusic(){
        try{
            if (!mediaPlayer.isPlaying()){
                mediaPlayer.start();
                System.out.println("continueMusic just called");
            }
        } catch (IllegalStateException e){
            e.printStackTrace();
        }

    }
}
