package com.datougames.mpuzzle.mainGame;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class LevelSettings {

    private static final String TAG = "HallOfFameData ";
    private final FirebaseFirestore db;

    private LinkedHashMap<String, Integer> levelSettings;

    public LevelSettings(FirebaseFirestore db){
        this.db = db;

        setup();
    }

    public void setup() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        db.setFirestoreSettings(settings);
    }

    public void getLevelSettingsDocument(){

        db.collection("levelSettings").document("iX4lT2Hjady64V47noNG")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {

                            DocumentSnapshot document = task.getResult();

                            levelSettings = new LinkedHashMap<String, Integer>();

                            if (document.exists()) {
                                if (document.get("maximumNumberOfLevels") != null){
                                    levelSettings.put("maximumNumberOfLevels", Integer.parseInt(String.valueOf(document.get("maximumNumberOfLevels"))));
                                }
                            }

                        }
                    }
                });
    }

    public HashMap<String, Integer> getLevelSettings(){
        return levelSettings;
    }

}
