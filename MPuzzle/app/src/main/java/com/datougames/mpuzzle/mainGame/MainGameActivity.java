package com.datougames.mpuzzle.mainGame;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.datougames.mpuzzle.BackgroundSoundService;
import com.datougames.mpuzzle.MainMenuActivity;
import com.datougames.mpuzzle.R;
import com.datougames.mpuzzle.marathon.MarathonData;
import com.datougames.mpuzzle.marathon.MarathonSettings;
import com.datougames.mpuzzle.userData.UpdateUserData;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

public class MainGameActivity extends AppCompatActivity implements RewardedVideoAdListener {

    public static MainGameActivity instance;
    public MainGameView mainGameView;
    public TextView timerText;
    public TextView levelNumber;
    public long currentLevel;
    public boolean mustReadFromDB;
    public UpdateUserData updateUserData;
    public MarathonData marathonData;
    public MarathonSettings marathonSettings;
    public boolean isMarathon;

    private final String TAG = "MainGameActivity: ";
    private FirebaseFirestore db;
    private String userId;
    private String userDisplayName;
    private Intent musicIntent;
    private RewardedVideoAd mRewardedVideoAd;

    public int marathonTimeLimit;
    public int rewardedTimeLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate() CALLED");

        instance = this;
        mustReadFromDB = getIntent().getBooleanExtra("mustReadFromDB", true);
        isMarathon = getIntent().getBooleanExtra("isMarathon", false);

        //MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();

        if (mustReadFromDB){
            db = FirebaseFirestore.getInstance();
            userId = MainMenuActivity.instance.getPersonId();
            updateUserData = new UpdateUserData(db);

            try {
                //identification number = 1 means that the request for read in db comes from MainGameActivity
                updateUserData.getDocument(userId, 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else if (isMarathon){

            db = FirebaseFirestore.getInstance();
            userId = MainMenuActivity.instance.getPersonId();
            userDisplayName = MainMenuActivity.instance.getUserDisplayName();

            marathonData = new MarathonData(db);
            marathonSettings = new MarathonSettings(db);
            marathonSettings.readTimeSettings();
        }
        else {
            currentLevel = getIntent().getLongExtra("currentLevel", currentLevel);
            setupActivity(currentLevel);
        }
    }

    public void setupMarathon(){
        try {
            marathonData.getDocument(userId, userDisplayName, 1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        musicIntent = new Intent(MainGameActivity.this, BackgroundSoundService.class);
        startMusicService();

        System.out.println("onStart()" + currentLevel);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMusicService();

        System.out.println("onStop()" + currentLevel);
    }

    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();

        if (MainMenuActivity.instance.playMusic){
            if (MainMenuActivity.instance.getBackgroundMusic() != null){
                MainMenuActivity.instance.getBackgroundMusic().continueMusic();
            }
        }
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause(this);
        super.onPause();

        if (MainMenuActivity.instance.playMusic){
            if (MainMenuActivity.instance.getBackgroundMusic() != null){
                MainMenuActivity.instance.getBackgroundMusic().pauseMusic();
            }
        }
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }

    public void startMusicService(){
        //startService(musicIntent);
        bindService(musicIntent, MainMenuActivity.instance.connection, Context.BIND_AUTO_CREATE);
    }

    public void stopMusicService(){
        //stopService(musicIntent);
        unbindService(MainMenuActivity.instance.connection);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setupActivity(long level){

        currentLevel = level;

        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main_game);

        updateVisibleLevelInfo();

        timerText = (TextView) findViewById(R.id.timer_text);
        mainGameView = (MainGameView) findViewById(R.id.table);
        mainGameView.createModelCorrespondingToLevel(timerText, (int)level, isMarathon);
        getModel();
    }

    public void updateVisibleLevelInfo(){
        levelNumber = (TextView) findViewById(R.id.level_text);
        levelNumber.setText("LEVEL: " + Long.toString(currentLevel));
    }

    public int getLevelNumber(){
        return (int)currentLevel;
    }

    public MainGameModel getModel() {
        MainGameView mainGameView = (MainGameView) findViewById(R.id.table);
        if (mainGameView.model == null){
            System.out.println("NULL MODEL");
            mainGameView.model = new MainGameModel(8, this.timerText, getLevelNumber());
        }
        return mainGameView.model;
    }

    public HashMap<String, Integer> getMetrics(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        HashMap<String, Integer> screenDimensions = new HashMap<String, Integer>();

        screenDimensions.put("width", width);
        screenDimensions.put("height", height);

        return screenDimensions;
    }

    public void onRightArrowPressed(View rightArrowButtonView){
        mainGameView.model.setPlayerPosition("right");
        //by calling invalidate() I control the update of the onDraw() method of MainGameView
        mainGameView.invalidate();
    }

    public void onDownArrowPressed(View downArrowButtonView){
        mainGameView.model.setPlayerPosition("down");
        //by calling invalidate() I control the update of the onDraw() method of MainGameView
        mainGameView.invalidate();
    }

    public void onBackButtonPressed(View backButtonView){
        mainGameView.model.stopCurrentTimeCounter();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //Dialog menu show up when back key is pressed
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            mainGameView.model.stopCurrentTimeCounter();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean islost(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Time's up. You lost!");
        alertDialog.setMessage("Play Again?");
        alertDialog.setCancelable(false); // preventing the dismiss on pressing the back button
        alertDialog.setCanceledOnTouchOutside(false); // preventing the dismiss on touching outside of alert dialog

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mainGameView.model.resetGame();
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        alertDialog.show();
        return true;
    }

    public void askForRewardedTime(){

        boolean hasInternet = isNetworkAvailable();

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Time's up!");

        if (hasInternet){
            alertDialog.setMessage("Would you like more time by watching an ad?");

            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (isNetworkAvailable()){
                        marathonSettings.getAdditionalAdRewardTime();
                    }
                    else {
                        MainMenuActivity.instance.showNoInternetMessage();
                        finish();
                    }
                }
            });

            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
        else {
            alertDialog.setMessage("End of attempt: No Internet Connection");

            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Finish", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
        alertDialog.setCancelable(false); // preventing the dismiss on pressing the back button
        alertDialog.setCanceledOnTouchOutside(false); // preventing the dismiss on touching outside of alert dialog

        alertDialog.show();
    }

    public void askForInternet(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("No Internet Connection!");
        alertDialog.setMessage("Activate Internet and press 'Show Ad' or press 'Finish' to finish your attempt");
        alertDialog.setCancelable(false); // preventing the dismiss on pressing the back button
        alertDialog.setCanceledOnTouchOutside(false); // preventing the dismiss on touching outside of alert dialog

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Show Ad", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (isNetworkAvailable()){
                    marathonSettings.getAdditionalAdRewardTime();
                }
                else {
                    askForInternet();
                }
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Finish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        alertDialog.show();
    }

    public void showFinishLevel(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setCancelable(false); // preventing the dismiss on pressing the back button
        alertDialog.setCanceledOnTouchOutside(false); // preventing the dismiss on touching outside of alert dialog

        if (mustReadFromDB && currentLevel < mainGameView.model.maximumPossibleLevel){
            alertDialog.setTitle("Congrats! You finish this level");
            alertDialog.setMessage("Go to the next level?");

            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mainGameView.createModelCorrespondingToLevel(timerText, (int)++currentLevel, false);
                    updateDataInDB();
                }
            });

            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ++currentLevel;
                    updateDataInDB();
                    finish();
                }
            });
        }
        else if (mustReadFromDB && currentLevel == mainGameView.model.maximumPossibleLevel){
            alertDialog.setTitle("Congrats! You finished the last level. New levels coming soon...");
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
        else {
            alertDialog.setTitle("Congrats! You finish this level");
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
        alertDialog.show();
    }

    public void updateDataInDB(){
        updateUserData.updateUserWithLevel(userId, currentLevel);

        showAdd();
    }

    public void shareFinishedLevel(){

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Share");
        alertDialog.setMessage("Would you like to share your score?");
        alertDialog.setCancelable(false); // preventing the dismiss on pressing the back button
        alertDialog.setCanceledOnTouchOutside(false); // preventing the dismiss on touching outside of alert dialog

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "I just finished level: " + currentLevel + " on MPuzzle with score: " + mainGameView.model.getRemainingTime());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                showFinishLevel();
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showFinishLevel();
            }
        });
        alertDialog.show();
    }

    public void showAdd(){
        if (MainMenuActivity.instance.getNextAdd().isLoaded()) {
            MainMenuActivity.instance.getNextAdd().show();
        } else {
        }
    }

    public boolean mustRewarded;

    public void showRewardingVideoAdd(int rewardAdditionalTime){

        this.rewardedTimeLimit = rewardAdditionalTime;
        mustRewarded = false;
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
        else {
        }
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-0130784657161358/8640471105", new AdRequest.Builder().build());
        // MY: ca-app-pub-0130784657161358/8640471105
        // TEST: ca-app-pub-3940256099942544/5224354917
    }

    @Override
    public void onRewarded(RewardItem reward) {
        //Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " + reward.getAmount(), Toast.LENGTH_SHORT).show();
        // Reward the user.
        mustRewarded = true;
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        //Toast.makeText(this, "onRewardedVideoAdLeftApplication", Toast.LENGTH_SHORT).show();
        //Log.d(TAG, "onRewardedVideoAdLeftApplication()");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        //Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
        if (!mustRewarded){
            finish();

            System.out.println("mustRewarded" + mustRewarded);
        }
        else {
            mainGameView.model.levelNumber = (int) currentLevel;
            mainGameView.model.giveRewardedTime(rewardedTimeLimit);
        }
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        //Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        //Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {
        //Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onRewardedVideoAdOpened()");
    }

    @Override
    public void onRewardedVideoStarted() {
        //Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onRewardedVideoStarted()");
    }

    @Override
    public void onRewardedVideoCompleted() {
        //Toast.makeText(this, "onRewardedVideoCompleted", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onRewardedVideoCompleted()");
    }
}