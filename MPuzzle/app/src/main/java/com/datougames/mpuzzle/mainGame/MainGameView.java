package com.datougames.mpuzzle.mainGame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.datougames.mpuzzle.MainMenuActivity;

public class MainGameView extends View implements View.OnClickListener, View.OnTouchListener{

    private static String TAG = "MainGameView: ";

    public MainGameModel model;

    public MainGameActivity mainGameActivity;

    static int bg = Color.WHITE;
    static int bgGrid = Color.rgb(128, 203, 196);
    static int foOff = Color.RED;
    static int fgOn = Color.GREEN;

    public int levelNumber;

    static int playerPosColor = Color.BLUE;
    static int winPositionColor = Color.GREEN;
    static int freeSquares = Color.CYAN;
    static int pathColor = Color.CYAN;
    static int barrierSquares = Color.RED;

    static int[] cols = {foOff, fgOn};

    int size; //store the size of each grid cell
    int dim;

    int minLen;
    int gridSquareLen;
    int xOff;
    int yOff;

    TextView timerView;

    public MainGameView(Context context) {
        super(context);
        mainGameActivity = (MainGameActivity)context;
        setup(context, "Constructor 1");
    }

    public MainGameView(Context context, MainGameModel model) {
        super(context);
        this.model = model;
        mainGameActivity = (MainGameActivity)context;
        setup(context, "Constructor 1a");
    }

    public MainGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mainGameActivity = (MainGameActivity)context;
        setup(context, "Constructor 2");
    }

    public MainGameView(Context context, AttributeSet attrs, int defStyleAttr, int defStyle) {
        super(context, attrs, defStyleAttr, defStyle);
        mainGameActivity = (MainGameActivity)context;
        setup(context, "Constructor 3");
    }

    private void setup(Context context, String cons){
        System.out.println(TAG + cons);
        setOnTouchListener(this);
        setOnClickListener(this);
    }

    public void createModelCorrespondingToLevel(TextView timerView, int levelNumber, boolean isMarathon){
        this.timerView = timerView;
        this.levelNumber = levelNumber;

        if (isMarathon){
            model = new MainGameModel(12, timerView, this.levelNumber);
        }
        else {
            if (this.levelNumber == 1){
                model = new MainGameModel(2, timerView, this.levelNumber);
            }
            else if (this.levelNumber == 2){
                model = new MainGameModel(3, timerView, this.levelNumber);
            }
            else if (this.levelNumber >= 3 && this.levelNumber <= MainMenuActivity.instance.levelSettings.getLevelSettings().get("maximumNumberOfLevels")){
                model = new MainGameModel(levelNumber, timerView, this.levelNumber);
            }
        }

        checkModel();
    }

    private void checkModel() {

        if (model != null){
            dim = model.size;
        }
        else {
            System.out.println(TAG + "Null model...Creating a default model");
            dim = 5;
            model = new MainGameModel(dim, this.timerView, this.levelNumber);
        }
    }

    public void draw(Canvas g) {
        super.draw(g);

        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.FILL);
        p.setColor(bg);


        int screenWidth = MainGameActivity.instance.getMetrics().get("width");
        int screenHeight = MainGameActivity.instance.getMetrics().get("height");

        int minimumValue = (int) Math.min(screenWidth/3, screenHeight);

        ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        layoutParams.height = (int) (minimumValue*1.2);
        layoutParams.width = (int) (minimumValue*1.2);
        setLayoutParams(layoutParams);

        setGeometry();

        //draw view background
        g.drawRect(0, 0, getWidth(), getHeight(), p);

        //draw the grid background
        p.setColor(bgGrid);
        g.drawRoundRect(new RectF(xOff, yOff, xOff + gridSquareLen, yOff + gridSquareLen), dim, dim, p);

        //draw the switches
        for (int i = 0; i < model.table.length; i++){
            for (int j = 0; j < model.table.length; j++){
                int cx = xOff + size * i + size / 2;
                int cy = yOff + size * j + size / 2;

                if (model.table[i][j] == model.barriers.getIdForBarriers()){
                    p.setColor(barrierSquares);
                }
                else if (model.table[i][j] == model.pathPosition){
                    p.setColor(pathColor);
                }
                else if (model.table[i][j] == model.playerPosition){
                    p.setColor(playerPosColor);
                }
                else if (model.table[i][j] == model.winPosition){
                    p.setColor(winPositionColor);
                }
                else if (model.table[i][j] == 0){
                    p.setColor(freeSquares);
                }
                else {
                    p.setColor(cols[model.table[i][j]]);
                }

                drawTile(g, cx, cy, p);
            }
        }
    }

    private void drawTile(Canvas g, int cx, int cy, Paint p){

        int length = (size * 7) / 8;
        int rad = size / 6;

        if (levelNumber >= 10){
            g.drawCircle(cx, cy, rad*2, p);
        }
        else {
            int x = cx - length / 2;
            int y = cy - length / 2;

            RectF rect = new RectF(x, y, x + length, y + length);
            g.drawRoundRect(rect, rad, rad, p);
        }

    }

    private void setGeometry() {

        int midX = getWidth() / 2;
        int midY = getHeight() / 2;
        minLen = Math.min(getWidth(), getHeight());

        int maxLen = Math.max(getWidth(), getHeight());

        //gridSquareLen = (minLen / dim) * dim;
        gridSquareLen = maxLen;

        size = gridSquareLen / dim;
        xOff = midX - gridSquareLen / 2;
        yOff = midY - gridSquareLen / 2;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
