package com.datougames.mpuzzle.userData;

import java.util.Map;

public class UserData {

    private String email;
    private String userID;

    private int currentScore;

    private long currentLevel;

    // Map<Level, ScoreOfThatLevel>
    private Map<Integer, Integer> scoreOfEachLevel;

    public UserData(){

    }

    public UserData(String email, int currentScore, Map<Integer, Integer> scores) {

    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public Map<Integer, Integer> getScoreOfEachLevel() {
        return scoreOfEachLevel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public void setScoreOfEachLevel(Map<Integer, Integer> scoreOfEachLevel) {
        this.scoreOfEachLevel = scoreOfEachLevel;
    }

    public long getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(long currentLevel) {
        this.currentLevel = currentLevel;
    }
}
