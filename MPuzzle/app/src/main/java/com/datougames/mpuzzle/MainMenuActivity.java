package com.datougames.mpuzzle;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.datougames.mpuzzle.mainGame.LevelSettings;
import com.datougames.mpuzzle.mainGame.MainGameActivity;
import com.datougames.mpuzzle.hallOfFame.HallOfFameActivity;
import com.datougames.mpuzzle.ui.LoginActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.HashMap;

public class MainMenuActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView userName;
    private String personId;
    private String userDisplayName;
    private Intent musicIntent;
    private BackgroundSoundService backgroundSoundService;
    private boolean mBound = false;
    public boolean playMusic;
    public LevelSettings levelSettings;

    FirebaseFirestore db;

    public static MainMenuActivity instance;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(String.valueOf("ca-app-pub-0130784657161358/5290178212")); // test ads : ca-app-pub-3940256099942544/1033173712
                                                                               // my ads : ca-app-pub-0130784657161358/5290178212

        loadNewAd();

        instance = this;
        db = FirebaseFirestore.getInstance();

        //Read Level Settings from Firebase
        levelSettings = new LevelSettings(db);
        levelSettings.getLevelSettingsDocument();

        //Create activity's content view
        setContentView(R.layout.activity_main_menu);

        //Note: A Google account's email address can change, so don't use it to identify a user.
        // Instead, use the account's ID, which you can get on the client with GoogleSignInAccount.getId,
        // and on the backend from the sub claim of the ID token.
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            personId = acct.getId();
            userDisplayName = acct.getDisplayName();
            userName = findViewById(R.id.player_name_text);
            userName.setText("Welcome: " + userDisplayName);
        }
    }

    public void loadNewAd(){
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                backgroundSoundService.pauseMusic();
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                loadGame();
                backgroundSoundService.continueMusic();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            musicIntent = new Intent(MainMenuActivity.this, BackgroundSoundService.class);
            startService(musicIntent);
            startMusicService();
        }
        catch (Exception e){
            System.out.println("Exception CAUGHT at onStart(): " + Arrays.toString(e.getStackTrace()));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopMusicService();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startMusicService();
        loadNewAd();
        levelSettings.getLevelSettingsDocument();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(musicIntent);
    }

    public void startMusicService(){
        try {
            startService(musicIntent);
            System.out.println("Bind Service");
            bindService(musicIntent, connection, Context.BIND_AUTO_CREATE);
        }
        catch (Exception e){
            System.out.println("Exception CAUGHT at startMusicService(): " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void stopMusicService(){

        try {
            System.out.println("Unbind Service");
            unbindService(connection);
        }
        catch (Exception e){
            System.out.println("Exception CAUGHT at stopMusicService(): " + e.getStackTrace());
        }

    }

    //Read the preferences of the player
    public void readPreferences(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this /* Activity context */);
        //default value for the playMusic preference is 'true'.
        Boolean playMusic = sharedPreferences.getBoolean("switch", true);
        sharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);

        this.playMusic = false;

        if (mBound){
            if (playMusic){
                backgroundSoundService.continueMusic();
                this.playMusic = true;
            }
            else {
                backgroundSoundService.pauseMusic();
                this.playMusic = false;
            }
        }
    }

    //Add a listener to the shared preferences in order to notify for any change and call the appropriate method for the change
    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("switch")){
                Boolean playMusic = sharedPreferences.getBoolean("switch", true);
                if (mBound){
                    if (playMusic){
                        backgroundSoundService.continueMusic();
                    }
                    else {
                        backgroundSoundService.pauseMusic();
                    }
                }
            }
        }
    };

    public String getPersonId(){
        return this.personId;
    }

    public String getUserDisplayName(){
        return this.userDisplayName;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //Dialog menu show up when back key is pressed
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
            isQuit();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void quitButtonPressed(View view){
        isQuit();
    }

    public boolean isQuit(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("You are going to close the game.");
        alertDialog.setMessage("Are you sure?");

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mBound = false;
                finish();
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // DO NOTHING!
            }
        });

        alertDialog.show();
        return true;
    }

    public void playButtonPressed(View view){
        if (isNetworkAvailable()){
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
            }
        }
        else {
            showNoInternetMessage();
        }
    }

    public InterstitialAd getNextAdd(){
        return mInterstitialAd;
    }

    public void loadGame(){
        if (isNetworkAvailable()){
            Intent intent = new Intent(this, MainGameActivity.class);
            startActivity(intent);
        }
        else {
            showNoInternetMessage();
        }
    }

    public void levelsButtonPressed(View view){
        if (isNetworkAvailable()){
            Intent intent = new Intent(this, LevelsActivity.class);
            startActivity(intent);
        }
        else {
            showNoInternetMessage();
        }
    }

    public void settingsButtonPressed(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void scoresButtonPressed(View view){
        if (isNetworkAvailable()){
            Intent intent = new Intent(this, ScoresActivity.class);
            startActivity(intent);
        }
        else {
            showNoInternetMessage();
        }
    }

    public void hallOfFameButtonPressed(View view){
        if (isNetworkAvailable()){
            Intent intent = new Intent(this, HallOfFameActivity.class);
            startActivity(intent);
        }
        else {
            showNoInternetMessage();
        }
    }

    public void howToPlayButtonPressed(View view){
        Intent intent = new Intent(this, HowToPlayActivity.class);
        startActivity(intent);
    }

    public void signOutButtonPressed(View view){
        LoginActivity.instance.signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void marathonModeButtonPressed(View view){
        if (isNetworkAvailable()){
            Intent intent = new Intent(this, MainGameActivity.class);
            intent.putExtra("isMarathon", true);
            intent.putExtra("mustReadFromDB", false);
            startActivity(intent);
        }
        else {
            showNoInternetMessage();
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    public ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // I've bound to BackgroundSoundService, cast the IBinder and get BackgroundSoundService instance
            BackgroundSoundService.LocalBinder binder = (BackgroundSoundService.LocalBinder) service;
            backgroundSoundService = binder.getService();
            mBound = true;

            readPreferences();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            System.out.println("Service Stopped");
            mBound = false;
        }
    };

    public BackgroundSoundService getBackgroundMusic(){
        return backgroundSoundService;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showNoInternetMessage(){
        Context context = getApplicationContext();
        CharSequence text = "You lost Internet connection!";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void showDeadlineMessage(){
        Context context = getApplicationContext();
        CharSequence text = "No Internet: You have 5 seconds to connect on Internet!";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void showComingSoonMessage(){
        Context context = getApplicationContext();
        CharSequence text = "Coming Soon!";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public double getMetrics(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        HashMap<String, Integer> screenDimensions = new HashMap<String, Integer>();

        screenDimensions.put("width", width);
        screenDimensions.put("height", height);

        //return screenDimensions;

        double density = displayMetrics.density * 160;
        double x = Math.pow(displayMetrics.widthPixels / density, 2);
        double y = Math.pow(displayMetrics.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);

        return screenInches;
    }
}
