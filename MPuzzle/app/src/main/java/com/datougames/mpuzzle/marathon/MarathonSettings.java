package com.datougames.mpuzzle.marathon;

import androidx.annotation.NonNull;

import com.datougames.mpuzzle.mainGame.MainGameActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MarathonSettings {

    private final FirebaseFirestore db;
    private static final String TAG = "MarathonSettings: ";

    private int timeLimit;
    private int rewardAdditionalTime;

    public MarathonSettings(FirebaseFirestore db){
        this.db = db;
    }

    public void readTimeSettings(){
        DocumentReference docRef = db.collection("timeLimitSettings").document("aQ6cbmdnD1SnydxrSjki");

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        if (document.get("marathonTime") != null){
                            timeLimit =  Integer.parseInt(String.valueOf(document.get("marathonTime")));
                        }
                        else {
                            timeLimit = 7000;
                        }

                        MainGameActivity.instance.marathonTimeLimit = timeLimit;
                        MainGameActivity.instance.setupMarathon();
                        //MainGameActivity.instance.getModel().getMarathonTimeLimit(timeLimit);

                    } else {
                    }
                } else {
                }
            }
        });
    }

    public void getAdditionalAdRewardTime(){
        DocumentReference docRef = db.collection("timeLimitSettings").document("aQ6cbmdnD1SnydxrSjki");

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        if (document.get("additionalTimeReward") != null){
                            rewardAdditionalTime =  Integer.parseInt(String.valueOf(document.get("additionalTimeReward")));
                        }
                        else {
                            rewardAdditionalTime = 2000;
                        }
                        //MainGameActivity.instance.rewardedTimeLimit = rewardAdditionalTime;
                        MainGameActivity.instance.showRewardingVideoAdd(rewardAdditionalTime);

                    } else {
                    }
                } else {
                }
            }
        });
    }

}
